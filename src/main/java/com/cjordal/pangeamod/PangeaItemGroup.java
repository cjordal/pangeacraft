package com.cjordal.pangeamod;

import com.cjordal.pangeamod.lists.block_list;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class PangeaItemGroup extends ItemGroup
{
    public PangeaItemGroup() {
        super("pangea");
    }

    @Override
    public ItemStack createIcon() {
        return new ItemStack(Item.BLOCK_TO_ITEM.get(block_list.sands_of_time));
    }
}
