package com.cjordal.pangeamod;

import com.cjordal.pangeamod.lists.block_list;
import com.cjordal.pangeamod.lists.item_list;
import com.cjordal.pangeamod.world.OreGeneration;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@Mod("pangeamod")
public class PangeaMod
{

    public static PangeaMod instance;
    public static final String modid = "pangeamod";
    private static final Logger logger = LogManager.getLogger(modid);

    public PangeaMod() {
        instance = this;

        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::clientRegistries);

        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event) {
        OreGeneration.setupOreGeneration();
        logger.info("setup method registered");
    }

    private void clientRegistries(final FMLClientSetupEvent event) {
        logger.info("clientRegistries method registered");
    }

    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents
    {

        @SubscribeEvent
        public static void registerBlocks(final RegistryEvent.Register<Block> event) {
            event.getRegistry().registerAll(
                    block_list.fern_wood.setRegistryName(location("fern_wood")),
                    block_list.sands_of_time.setRegistryName(location("sands_of_time")),
                    block_list.lead_ore.setRegistryName(location("lead_ore")),
                    block_list.lead_block.setRegistryName(location("lead_block"))
            );
            logger.info("blocks registered");
        }

        @SubscribeEvent
        public static void registerItems(final RegistryEvent.Register<Item> event) {
            event.getRegistry().registerAll(
                    // items
                    item_list.t_rex_tooth.setRegistryName(location("t_rex_tooth")),
                    item_list.lead_ingot.setRegistryName(location("lead_ingot")),
                    // blocks
                    item_list.fern_wood.setRegistryName(block_list.fern_wood.getRegistryName()),  // <<<<<<< could fail, but doesn't
                    item_list.sands_of_time.setRegistryName(block_list.sands_of_time.getRegistryName()),
                    item_list.lead_ore.setRegistryName(block_list.lead_ore.getRegistryName()),
                    item_list.lead_block.setRegistryName(block_list.lead_block.getRegistryName()),
                    // tools
                    item_list.lead_axe.setRegistryName(location("lead_axe")),
                    // armor
                    item_list.lead_helmet.setRegistryName(location("lead_helmet")),
                    item_list.lead_chestplate.setRegistryName(location("lead_chestplate")),
                    item_list.lead_leggings.setRegistryName(location("lead_leggings")),
                    item_list.lead_shoes.setRegistryName(location("lead_shoes"))
            );
            logger.info("items registered");
        }

        private static ResourceLocation location(String name) {
            return new ResourceLocation(modid, name);
        }

    }

}
