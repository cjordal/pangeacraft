package com.cjordal.pangeamod.lists;

import com.cjordal.pangeamod.PangeaMod;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.IArmorMaterial;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;

public enum ArmorMaterialList implements IArmorMaterial
{
    lead_armor(20, new int[]{2, 7, 4, 2}, 50, "item.armor.equip_generic",
            item_list.lead_ingot, "lead_armor", 0.0f);

    private static final int[] ARMOR_DURABILITY = {2, 7, 4, 2};

    private int durability, enchantability;
    private int[] damage_reduction;
    private float toughness;
    private String name, sound;
    private Item repair_material;

    private ArmorMaterialList(int durability, int[] damage_reduction, int enchantability,
                              String sound, Item repair_material, String name,
                              float toughness)
    {
        this.durability = durability;
        this.damage_reduction = damage_reduction;
        this.enchantability = enchantability;
        this.sound = sound;
        this.repair_material = repair_material;
        this.name = name;
        this.toughness = toughness;
    }

    @Override
    public int getDurability(EquipmentSlotType slotIn) {
        return durability * ARMOR_DURABILITY[slotIn.getIndex()];
    }

    @Override
    public int getDamageReductionAmount(EquipmentSlotType slotIn) {
        return damage_reduction[slotIn.getIndex()];
    }

    @Override
    public int getEnchantability() {
        return enchantability;
    }

    @Override
    public SoundEvent getSoundEvent() {
        return new SoundEvent(new ResourceLocation(sound));
    }

    @Override
    public Ingredient getRepairMaterial() {
        return Ingredient.fromItems(repair_material);
    }

    @Override
    public String getName() {
        return PangeaMod.modid + ":" + name;
    }

    @Override
    public float getToughness() {
        return toughness;
    }
}
