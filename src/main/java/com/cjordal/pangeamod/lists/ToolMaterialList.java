package com.cjordal.pangeamod.lists;

import net.minecraft.item.IItemTier;
import net.minecraft.item.Item;
import net.minecraft.item.crafting.Ingredient;

public enum ToolMaterialList implements IItemTier
{
    lead_tools(30, 3.0f, 4.0f, 1, 50, item_list.lead_ingot);

    int max_uses, harvest_level, enchantability;
    float efficiency, attack_damage;
    Item repair_material;

    private ToolMaterialList(int max_uses, float efficiency, float attack_damage,
                                int harvest_level, int enchantability, Item repair_material) {
        this.max_uses = max_uses;
        this.efficiency = efficiency;
        this.attack_damage = attack_damage;
        this.harvest_level = harvest_level;
        this.enchantability = enchantability;
        this.repair_material = repair_material;
    }

    @Override
    public int getMaxUses() {
        return max_uses;
    }

    @Override
    public float getEfficiency() {
        return efficiency;
    }

    @Override
    public float getAttackDamage() {
        return attack_damage;
    }

    @Override
    public int getHarvestLevel() {
        return harvest_level;
    }

    @Override
    public int getEnchantability() {
        return enchantability;
    }

    @Override
    public Ingredient getRepairMaterial() {
        return Ingredient.fromItems(repair_material);
    }
}
