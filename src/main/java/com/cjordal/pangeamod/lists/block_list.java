package com.cjordal.pangeamod.lists;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class block_list
{
    public static Block fern_wood = new Block(
            Block.Properties
                    .create(Material.WOOD)
                    .hardnessAndResistance(1.2f)
                    .sound(SoundType.WOOD)
    );
    public static Block sands_of_time = new Block(
            Block.Properties
                    .create(Material.SAND)
                    .hardnessAndResistance(0.5f)
                    .sound(SoundType.SAND)
    );
    public static Block lead_ore = new Block(
            Block.Properties
                .create(Material.ROCK)
                .hardnessAndResistance(3.0f)
                .sound(SoundType.STONE)
    );
    public static Block lead_block = new Block(
            Block.Properties
                .create(Material.IRON)      // THIS SHOULD PROBABLY CHANGE
                .hardnessAndResistance(4.0f)
                .sound(SoundType.METAL)
    );
}
