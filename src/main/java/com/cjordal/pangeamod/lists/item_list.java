package com.cjordal.pangeamod.lists;

import com.cjordal.pangeamod.PangeaItemGroup;
import net.minecraft.inventory.EquipmentSlotType;
import net.minecraft.item.*;

public class item_list
{
    // item groups
    public static final ItemGroup pangea_group = new PangeaItemGroup();

    // items
    public static Item t_rex_tooth = new Item(
            new Item.Properties().group(pangea_group)
    );
    public static Item lead_ingot = new Item(
            new Item.Properties().group(pangea_group)
    );

    // block items
    public static Item fern_wood = new BlockItem(
            block_list.fern_wood,
            new Item.Properties().group(pangea_group)
    );
    public static Item sands_of_time = new BlockItem(
            block_list.sands_of_time,
            new Item.Properties().group(pangea_group)
    );
    public static Item lead_ore = new BlockItem(
            block_list.lead_ore,
            new Item.Properties().group(pangea_group)
    );
    public static Item lead_block = new BlockItem(
            block_list.lead_block,
            new Item.Properties().group(pangea_group)
    );

    // tools
    public static Item lead_axe = new AxeItem(
            ToolMaterialList.lead_tools,
            1.0f,       // added damage
            -3.6f,      // added speed
            new Item.Properties().group(pangea_group)
    );

    // armor
    public static Item lead_helmet = new ArmorItem(
            ArmorMaterialList.lead_armor,
            EquipmentSlotType.HEAD,
            new Item.Properties().group(pangea_group)
    );
    public static Item lead_chestplate = new ArmorItem(
            ArmorMaterialList.lead_armor,
            EquipmentSlotType.CHEST,
            new Item.Properties().group(pangea_group)
    );
    public static Item lead_leggings = new ArmorItem(
            ArmorMaterialList.lead_armor,
            EquipmentSlotType.LEGS,
            new Item.Properties().group(pangea_group)
    );
    public static Item lead_shoes = new ArmorItem(
            ArmorMaterialList.lead_armor,
            EquipmentSlotType.FEET,
            new Item.Properties().group(pangea_group)
    );
}
