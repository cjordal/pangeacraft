package com.cjordal.pangeamod.world;

import com.cjordal.pangeamod.lists.block_list;
import net.minecraft.world.biome.Biome;
import net.minecraft.world.gen.GenerationStage;
import net.minecraft.world.gen.feature.Feature;
import net.minecraft.world.gen.feature.OreFeatureConfig;
import net.minecraft.world.gen.placement.CountRangeConfig;
import net.minecraft.world.gen.placement.Placement;
import net.minecraftforge.registries.ForgeRegistries;

public class OreGeneration
{
    private static final CountRangeConfig ST_SPAWN_RANGE = new CountRangeConfig(
            1000,       // vein count
            40,         // min height
            0,          // max height base
            128         // max height
    );
    private static final int ST_VEIN_SIZE = 7;

    public static void setupOreGeneration() {
        // place sands of time everywhere but the nether and end
        for (Biome biome : ForgeRegistries.BIOMES) {
            Biome.Category biome_type = biome.getCategory();
            if ((biome_type != Biome.Category.THEEND) && (biome_type != Biome.Category.NETHER)) {
                biome.addFeature(
                        GenerationStage.Decoration.UNDERGROUND_ORES,
                        Biome.createDecoratedFeature(
                                Feature.ORE,
                                new OreFeatureConfig(
                                        OreFeatureConfig.FillerBlockType.NATURAL_STONE,
                                        block_list.lead_ore.getDefaultState(),
                                        ST_VEIN_SIZE
                                ),
                                Placement.COUNT_RANGE,
                                ST_SPAWN_RANGE
                        )
                );
            }
        }
    }
}
